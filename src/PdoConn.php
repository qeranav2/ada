<?php

namespace Ada;

use Ada\Exception\AdaException;
use Exception;
use PDO;

class PdoConn extends PDO
{

    private static $instance = null;

    public function __construct()
    {

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, // error exception mode
            PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" // utf8 always
        ];

        try {
            parent::__construct('mysql:host=' .$_ENV['DB_HOSTNAME'] .
                ';port=' . $_ENV['DB_PORT'] . ';dbname=' .
                $_ENV['DB_SCHEMA'], $_ENV['DB_USER'], $_ENV['DB_PASSWORD'], $options);
        } catch (Exception $ex) {

            throw new AdaException($ex->getMessage());
        }
    }


    public static function singleton(): ?PdoConn
    {
        if (self::$instance == null) {

            self::$instance = new self();
        }

        return self::$instance;
    }

}
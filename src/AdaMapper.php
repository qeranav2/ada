<?php

namespace Ada;

abstract class AdaMapper
{
    protected $entity;
    protected $pk;

    public function __construct(string $entity, string $pk)
    {
        $this->entity = $entity;
        $this->pk = $pk;
    }

    /**
     * @param array $rows
     * @return array
     */
    protected function createEntities(array $rows = []): array
    {

        $entities = [];

        if ($rows) {
            foreach ($rows as $row):
                $entities[] = $this->createEntity($row);
            endforeach;
        }

        return $entities;
    }


    /**
     * @param array $row
     * @return mixed
     */
    protected function createEntity(array $row)
    {

        return new $this->entity($row);

    }


}

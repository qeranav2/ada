<?php

namespace Ada;

use Ada\Exception\AdaException;

trait AdaEntityTrait
{
    /**
     * @var array entity required attributes
     */
    protected $required = [];


    /**
     * Create entity
     * @param array $data
     */
    public function populate(array $data)
    {

        foreach ($data as $k => $v) {
            $this->__set($k, $v);
        }
    }


    /**
     * Update entity attributes
     * @param array $attributes
     * @param bool $only_setted
     */
    public function updateAttributes(array $attributes, bool $only_setted = true)
    {
        foreach ($attributes as $attr => $value) {

            if ($only_setted) {
                if (!empty($value)) {
                    $this->__set($attr, $value);
                }
            } else {
                $this->__set($attr, $value);
            }

        }

    }


    /**
     * @param $prop
     * @return mixed
     */
    public function __get($prop)
    {
        if (is_object($prop)) {
            return $prop;
        }

        if (!property_exists($this, $prop)) {
            throw new AdaException(sprintf('Property %s dont exist on "getProperty"', $prop));
        }

        $accesor = $this->mutator($prop, 'get');
        // if exist the setter
        if (method_exists($this, $accesor) and is_callable([$this, $accesor])) {
            $ret = $this->$accesor();
        } else {
            $ret = $this->$prop;
        }

        return $ret;
    }


    /**
     * Setter mutator
     *
     * @param $name
     * @param $value
     * @return $this
     */
    public function __set($name, $value)
    {
        if (property_exists($this, $name)) {
            // build the setter
            $mutator = $this->mutator($name, 'set');
            // if exist the setter
            if (method_exists($this, $mutator) and is_callable([$this, $mutator])) {
                $this->$mutator($value);
            }
        }

        return $this;
    }

    /**
     * @param $name
     * @param string $method
     * @return string
     */
    private function mutator($name, string $method = 'set'): string
    {
        $attr_parts = explode('_', $name);
        // build the mutator
        $mutator = $method . ucfirst($attr_parts[0]);
        $mutator .= (isset($attr_parts[1])) ? ucfirst($attr_parts[1]) : '';

        return $mutator;
    }

    /**
     * Serialize entity to array
     * @param array $exclude
     * @return mixed
     */
    public function __toArray(array $exclude = [])
    {
        $array = json_decode(json_encode($this), true);

        foreach ($exclude as $field) {
            unset($array[$field]);
        }
        unset($array['required']);

        return $array;
    }

    /**
     * @return array
     */
    public function jsonSerialize(): array
    {
        $properties = get_object_vars($this);

        $json_result = [];

        foreach ($properties as $k => $v):

            $pos = strpos($k, '_');

            if (is_numeric($pos) and $pos == 0) {
                $key = substr($k, 1);
                $json_result[$key] = $this->__get($key);
            } else {
                $json_result[$k] = $v;
            }
        endforeach;
        return $json_result;
    }



    public static function hasOne(string $related, $fields,$callback = null, $repository = null)
    {
      //@todo

    }

}
<?php

namespace Ada;

use Ada\Adapter\PDOAdapter;
use Exception;

abstract class AdaRepository extends AdaMapper
{

    private $adapter;

    public function __construct(string $table, $fqdn, $pk, $conn = null)
    {
        parent::__construct($fqdn, $pk);
        $this->adapter = new PDOAdapter(PdoConn::singleton(), $table);
    }


    /**
     * @throws Exception
     */
    public function findAll(array $conditions = [], array $options = []): array
    {

        $rows = $this->adapter->find($conditions, $options);
        return $this->createEntities($rows);

    }


    /**
     * @throws Exception
     */
    protected function save($object)
    {
        $data = $object->__toArray();
        $id = $this->pk;

        if (is_null($object->$id)) {
            $object->$id = $this->adapter->insert($data);
        } else {
            $this->adapter->update($data, ["$id" => $object->$id]);
        }
    }

    /**
     * @param string $sql
     * @param array $binds
     * @param string $fetch
     * @return array
     * @throws Exception
     */
    public function findBySql(string $sql, array $binds = [], string $fetch = 'all'): array
    {

        $rows = $this->adapter->selectByQuery($sql, $binds, $fetch);
        return $this->createEntities($rows);

    }


    /**
     * @param array $conditions
     * @param array $options
     * @return mixed|null
     * @throws Exception
     */
    public function findOne(array $conditions = [], array $options = [])
    {
        $row = $this->adapter->findOne($conditions, $options);
        if ($row) {
            return $this->createEntity($row);
        }
        return null;
    }

    /**
     * @throws Exception
     */
    public function remove(array $condition)
    {
        return $this->adapter->delete($condition);
    }

}